# Overview

This Ansible code was created to install all needed applications in the Mobica Workshop for AWS Workspace machines.

## List of supported applications
Installed by using package manager (apt):
- ansible
- docker
- docker-compose
- ruby
- kubectl
- helm
- golang-go
- nodejs
- kubectx
- trivy

Downloaded as a binary file:
- kube-capacity
- k9s
- grype

Installed by using PIP:
- pipenv

Installed by using official script:
- k3d

According to https://mobica-workshops.gitlab.io/documentation/requirements-guide/

## Requirements
AWS Workspace instance with Ubuntu 22.04 image


## Usage

To install all packages on your AWS Workspace machine:

1. Add your ip address and user name to inventory.ini file.

2. Run the command:
```bash
ansible-playbook -i inventory.ini workshop_apps.yml
```
